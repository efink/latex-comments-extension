/*
 * Allow latex formula like $ \sum_i^n{x_i} $
 * or
 * $$ \sum_i^n{x_i} $$
 */
function latexifyAll() {

  var head = document.getElementsByTagName("head")[0], script;
  script = document.createElement("script");
  script.type = "text/x-mathjax-config";
  script[(window.opera ? "innerHTML" : "text")] =
    "MathJax.Hub.Config({\n" +
    "  tex2jax: { inlineMath: [['$','$'], ['\\\\(','\\\\)']], \n" +
    "             skipTags: ['script','noscript','style','textarea','code']}" +
    "});" +
    "var MutationObserver = (window.MutationObserver ||" +
    "                        window.WebKitMutationObserver);\n" +
    "var observer = new MutationObserver(function(mutations, observer) {\n" +
    "  // fired when a mutation occurs\n" +
    "  // console.log(mutations, observer);\n" +
    "  mutations.forEach(function(mutation) {\n" +
    "    //console.log(mutation.type + mutation.target);\n" +
    "    if (mutation.target) {\n" +
    "      MathJax.Hub.Queue(['Typeset',MathJax.Hub, mutation.target]);\n" +
    "    };\n" +
    "  });\n" +
    "});\n" +
    "// define what element should be observed by the observer\n" +
    "// and what types of mutations trigger the callback\n" +
    "observer.observe( document.getElementById('repo-content'), {\n" +
    "  subtree: true,\n" +
    "  childList: true,\n" +
    "});";

  head.appendChild(script);
  script = document.createElement("script");
  script.type = "text/javascript";
  //script.src  = "https://c328740.ssl.cf1.rackcdn.com/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML";
  script.src  = "https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
  head.appendChild(script);
};

latexifyAll();
